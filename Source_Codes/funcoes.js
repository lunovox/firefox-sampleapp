﻿activity = null;

window.onload = function() {
	window.applicationCache.addEventListener('updateready', function(e) {
    if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
      // Browser downloaded a new app cache.
      // Swap it in and reload the page to get the new hotness.
      window.applicationCache.swapCache();
      if (confirm('A new version of this site is available. Load it?')) {
        window.location.reload();
      }
    } else {
      // Manifest didn't changed. Nothing new to server.
    }
  }, false);
	
	if(document.getElementsByTagName('canvas').length>=1){
		drawEcran();
	}else{
		navigator.mozSetMessageHandler('activity', function(activityRequest) {
			activity = activityRequest;
			if (activityRequest.source.name == 'share') {
				addImages(activityRequest.source.data);
			}
		});
	}

	//document.getElementById('ok').onclick = done;
};


// Check if a new cache is available on page load.
/*window.addEventListener('load', function(e) {

  window.applicationCache.addEventListener('updateready', function(e) {
    if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
      // Browser downloaded a new app cache.
      // Swap it in and reload the page to get the new hotness.
      window.applicationCache.swapCache();
      if (confirm('A new version of this site is available. Load it?')) {
        window.location.reload();
      }
    } else {
      // Manifest didn't changed. Nothing new to server.
    }
  }, false);

}, false);/**/


window.onresize = drawEcran;
function done() {
	activity.postResult('shared');
}


function addImages(data) {
  var blobs = data.blobs, filenames = data.filenames;
  console.log('share receiver: got', blobs.length, 'images');
  blobs.forEach(function(blob, index) {
    var url = URL.createObjectURL(blob);
    var img = document.createElement('img');
    img.style.width = '100px';
    img.src = url;
    img.onload = function() { URL.revokeObjectURL(url); };
    document.body.appendChild(img);
    var label = document.createElement('span');
    label.textContent = filenames[index];
    document.body.appendChild(label);
  });
}

//--------------------------------------------------------------------------------

function doBlob(){
	pick.onsuccess = function () {
		// Create image and set the returned blob as the src
		var img = document.createElement("img");
		img.src = window.URL.createObjectURL(this.result.blob);

		// Present that image in your app
		var imagePresenter = document.querySelector("#image-presenter");
		imagePresenter.appendChild(img);
	};

	pick.onerror = function () {
		// If an error occurred or the user canceled the activity
		alert("Can't view the image!");
	};
}

function doRegistraAtividade(){
	var register = navigator.mozRegisterActivityHandler({
		name: "view", 
		disposition: "inline", 
		filters: {
			type: "image/png"
		}
	});
	 
	register.onerror = function () {
		//console.log("Failed to register activity");
		alert("Failed to register activity");
	}
	//and then handle the activity:
	navigator.mozSetMessageHandler("activity", function (a) {
		var img = getImageObject();
		img.src = a.source.url;
		//Call a.postResult() or a.postError() if the activity should return a value?
	});
}/**/

function doDiscar(){
	var call = new MozActivity({
		name: "dial",
		data: {
			number: "08187310165"
		}
	});
}

function doSMS(){
	var sms = new MozActivity({
		name: "new",
		data: {
			type: "websms/sms",
			number: "08187310165"
		}
	});
}

function addContato(){
	var newContact = new MozActivity({
		name: "new",
		data: {
			type: "webcontacts/contact",
			params: { // Will possibly move to be direct properties under "data"
				giveName: "Lunovox",
				familyName: "Heavenfinder",
				tel: "08187310165",
				email: "rui.gravata@gmail.com",
				address: "Gravatá-PE",
				note: "É o programador deste Script",
				company: "TUATECnologia.COM.BR COOP. LTDA"
			}
		}
	});
}

function openSite(){
	var openURL = new MozActivity({
		name: "view",
		data: {
			type: "url", // Possibly text/html in future versions
			url: "http://www.tuatec.com.br"
		}
	});
}

function addFavorito(){
	var savingBookmark = new MozActivity({
		name: "save-bookmark",
		data: {
			type: "url",
			url: "http://www.tuatec.com.br",
			name: "TUATECnologia.COM.BR COOP. LTDA",
			icon: "http://www.tuatec.com.br/imagens/favicon.png"
		}
	});
}

function showFormShare(){
	window.location='frmShare.html';
}
function showTestPerformance(){
	window.location='frmTestPerformance.html';
}
function doVoltar(){
	window.location='frmPrincipal.html';
}

function drawEcran(){
	var context = document.getElementsByTagName('canvas')[0].getContext('2d');
	context.canvas.width  = window.innerWidth;
	context.canvas.height = window.innerHeight;
	var lastX = context.canvas.width * Math.random();
	var lastY = context.canvas.height * Math.random();
	var hue = 0;

	function line() {
		context.save();
		context.translate(context.canvas.width/2, context.canvas.height/2);
		context.scale(1, 1);
		context.translate(-context.canvas.width/2, -context.canvas.height/2);
		context.beginPath();
		context.lineWidth = 5 + Math.random() * 10;
		context.moveTo(lastX, lastY);
		lastX = context.canvas.width * Math.random();
		lastY = context.canvas.height * Math.random();
		context.bezierCurveTo(
			context.canvas.width * Math.random(),
			context.canvas.height * Math.random(),
			context.canvas.width * Math.random(),
			context.canvas.height * Math.random(),
			lastX, lastY
		);

		hue = hue + 20 * Math.random();
		context.strokeStyle = 'hsl(' + hue + ', 50%, 50%)';
		context.shadowColor = 'white';
		context.shadowBlur = 10;

		context.stroke();
		context.restore();
	}
	setInterval(line, 1);

	function blank() {
		context.fillStyle = 'rgba(0,0,0,0.1)';
		context.fillRect(0, 0, context.canvas.width, context.canvas.height);
		context.globalAlpha = 0.9;
	}
	setInterval(blank, 30);


	function image() {
	var img = new Image();  
		img.onload = function(){  
			context.drawImage(img,context.canvas.width * Math.random(),context.canvas.width * Math.random(), 90 + Math.random() * 90, 90 + Math.random() * 90);  
		}  
		img.src = 'imagens/icon_48x48_tmw.png'; 
	}		
	setInterval(image, 50);


	function textImage() {
	var img = new Image();  
		img.onload = function(){  
			context.drawImage(img,context.canvas.width * Math.random(),context.canvas.width * Math.random(), 90 + Math.random() * 90, 90 + Math.random() * 90);  
		}  
		img.src = 'imagens/tmw-maker.png'; // Set source path
	}
	setInterval(textImage, 150);
}

function doFechar(){
	if(confirm("Deseja realmente fechar esta aplicação?")){window.close();}
}
